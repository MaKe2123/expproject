﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Disaheim
{
    public class Utility
    {
        public double GetValueOfBook (Book book)
        {
            return book.Price;
        }

        public double GetValueOfAmulet (Amulet amulet)
            
        {
            double priceAmulet = 0;
            if (amulet.Quality == Level.low) 
            {
                priceAmulet = 12.5;
            }

            else if (amulet.Quality == Level.medium)
            {
                priceAmulet = 20.0;
            }

            else if (amulet.Quality == Level.high)
            {
                priceAmulet = 27.5;
            }

            return priceAmulet;
        }

    }
}
