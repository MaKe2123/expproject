﻿namespace hydac
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Hydac Guetsbook \n");

            bool run = true;

            while (run)
            {
                int date;
                int date1 = 20220101;
                int date2 = 20220102; //Hard coded date
                int date3 = 20220103; //Hard coded date

                Console.WriteLine("Press 1 for create guest \nPress 2 for view guest");
                string menu = Console.ReadLine();
               
                switch (menu)
                {
                    case "1":
                        Console.WriteLine("Write wanted date");
                        date = int.Parse(Console.ReadLine());

                        if (date == date1)
                            Console.WriteLine("Write your name");
                        string name = Console.ReadLine();
                        Console.WriteLine("Write your firm");
                        string firm = Console.ReadLine();
                        string guest1 = $"Name: {name} \nFirm: {firm}";                      

                        Console.WriteLine(guest1);
                        break;

                    case "2":
                        Console.WriteLine("Write wanted date");
                        date = int.Parse(Console.ReadLine());

                        if (date == date2) //View guest
                        {
                            string guest2 = "Name: Poul \nFirm: Bla"; //Hard coded guest
                            Console.WriteLine(guest2);
                        }
                        if (date == date3) //View guest
                        {
                            string guest3 = "Name: Sven \nFirm: BlueDa"; //Hard coded guest
                            Console.WriteLine(guest3);
                        }
                        break;

                    case "exit":
                    default:
                        run = false;
                        Console.WriteLine("Exit");
                        break;

                    case "":
                        Console.WriteLine("Indtast gyldigt nummer \n");
                        break;                       
                }

            }


        }
        
    }
}
